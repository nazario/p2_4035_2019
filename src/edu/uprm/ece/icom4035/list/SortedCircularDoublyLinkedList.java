/**
 * @author Victor A. Nazario Morales <victor.nazario@upr.edu>
 * SN: 843-15-4984
 * Gitlab: nazario
 */

package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {

	
	/**
	 * @author Victor A. Nazario Morales
	 * This class is an abstraction of a doubly linked node.
	 */
	
	private static class Node<E>{
		
		private E element; 
		private Node<E> prev;
		private Node<E> next; 
		
		public Node() {
			this.element = element;
			this.prev = null; 
			this.next = null;  
		}
		
		public Node(E e, Node<E> prev, Node<E> next) {
			this.element = e; 
			this.next = next;
			this.prev = prev; 
		}
		
		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		/**
		 * Given a node ntc, sets all its references to null and the data element to null
		 * @param ntc - the node to clean
		 */
		
		private void clean() {
			this.setElement(null);
			this.setPrev(null);
			this.setNext(null);
		}

	}
	
	
	/**
	 * @author Victor A. Nazario Morales
	 * This class represents a forward iterator on the list starting from index (inclusive).
	 * To iterate over the complete list, indicate index (starting point) as 0.
	 */
	
	private class ForwardListIterator implements Iterator<E>{
			
		private Node<E> current;
		
		/**
		 * Creates a new instance of the iterator going forward from the indicated index. 
		 * @param index - the inclusive starting point 
		 */
		
		public ForwardListIterator(int index) {
			if (index < 0 || index > size()-1)
				throw new IndexOutOfBoundsException("Index is not within the list's bounds.");
			
			current = header;
			
			for (int i = 0; i <= index; i++) {
				current = current.getNext();
			}
		}
		
		@Override
		public boolean hasNext() {
			return current.getElement() != null;
		}

		@Override
		public E next() {
			if (hasNext()) {
				E tbr = null;
				tbr = current.getElement();
				current = current.getNext();
				return tbr;
			}
			
			else {
				throw new NoSuchElementException("No more elements to iterate over");
			}
		}
		
	}
	
	
	/**
	 * @author Victor A. Nazario Morales
	 * This class represents a backward iterator on the list starting from index (inclusive).
	 * To iterate over the complete list, indicate index (starting point) as size-1.
	 */
	
	private class BackwardListIterator implements ReverseIterator<E>{
		
		Node<E> current; 
		
		/**
		 * Creates a new instance of the iterator going backward from the indicated index. 
		 * @param index - the inclusive starting point 
		 */
		
		public BackwardListIterator(int index) {
			if (index < 0 || index > size()-1)
				throw new IndexOutOfBoundsException("Index is not within the list's bounds.");
			
			current = header.getPrev();
			
			for (int i = size()-1; i >index ; i--) {
				current = current.getPrev();
			}
		}

		@Override
		public boolean hasPrevious() {
			return current.getElement() != null;
		}

		@Override
		public E previous() {
			if (hasPrevious()) {
				E etr = null;
				etr = current.getElement();
				current = current.getPrev();
				return etr;
			}
			
			else {
				throw new NoSuchElementException("No more elements to iterate over");
			}
			
		}	
		
	}

	
	private Node<E> header;   //Holds a reference to a sentinel first node
	private int size;        //Current size for the list instance
	
	
	//Constructor for the list instance
	
	/**
	 * Constructs a new instance of a Sorted Circular DLL
	 * @return a new instance of the specified list
	 */
	
	public SortedCircularDoublyLinkedList() {
		header = new Node<E>();
		
		header.setElement(null);
		header.setNext(header);
		header.setPrev(header);
		
		size = 0; 
	}
	
	
	/**
	 * Adds a new element to the list in the right order.
	 *  The method traverses the list, looking for the right position for obj.
	 *  @param obj - the object to be appended to the list 
	 *  @return true if the object is added successfully, false otherwise
	 */
	
	@Override
	public boolean add(E obj) {
		
		//Cases for whenever the node to be added is the first actual node or smaller
		//than the already existing first node
		
		if (this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj, header, header);
			header.setNext(newNode);
			header.setPrev(newNode);
			size++; 
			
			return true;
		}
		
		if(header.getNext().getElement().compareTo(obj) > 0) {
			Node<E> newNode = new Node<E>(obj, header, header.getNext());
			header.getNext().setPrev(newNode);
			header.setNext(newNode);
			
			size++; 
			
			return true;
		}
		
		//Case when the element to be added fits in the middle of the list instance
		Node<E> temp = header.getNext();
		
		for(int i = 0; i < size; i++) {
			if(temp.getElement().compareTo(obj) > 0) {
				Node<E> newNode = new Node<E>(obj, temp.getPrev(), temp);
				temp.getPrev().setNext(newNode);
				temp.setPrev(newNode);
				size++; 
				return true;
			}
			
			temp = temp.getNext();
		}
		
		//If the loop return statement does not execute, the node needs to be added at the
		//end of the list, thus the following set of instructions
		
		Node<E> newNode = new Node<E>(obj, header.getPrev(), header);
		header.getPrev().setNext(newNode);
		header.setPrev(newNode);
		size++;
		
		return true;
	}

	
	/** 
	 * Returns the size (number of elements) proper to the instance to which the method is called upon. 
	 * @return the number of elements on the list
	 */
	
	@Override
	public int size() {
		return size;
	}

	
	/**
	 * Removes the first occurrence of obj from the list. 
	 * @param The element to find and remove from the list
	 * @return true if the obj is removed, false otherwise
	 */

	@Override
	public boolean remove(E obj) {
		if (isEmpty())
			return false; 
		
		Node<E> temp = header.getNext();
		
		for (int i = 0; i < size; i++) {
			if (obj.equals(temp.getElement())) {
				removeNode(temp); 
				return true;
			}
			temp = temp.getNext(); 	
		}
		
		return false;
	}
	
	
	/**
	 * Removes the element at position index
	 * @param index- the index desired to remove
	 * @return Returns true if the element is deleted, or an IndexOutOfBoundsException if invalid
	 * @throws IndexOutOfBoundsException
	 */
	
	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException{
		if (index < 0 || index > size - 1) 
			throw new IndexOutOfBoundsException("The specified index is out of the list's bounds");
		
		int i = 0;
		Node<E> temp = header.getNext();
		
		//Loops until the index is found, when found that reference gets passed to
		//the removeNode helper method
		
		while (i != index) {
			temp = temp.getNext();
			i++;
		}
		
		removeNode(temp);
		
		return true;
	}

	
	/**
	 * Removes all instances of obj from the list
	 * @param obj - the object to be removed from the list
	 * @return count - the number of instances of obj that were removed from the list
	 */
	
	@Override
	public int removeAll(E obj) {
		int count = 0;
		
		while(contains(obj)) {
				count++;
				remove(obj);
		}
		
		return count;
	}

	
	/**
	 * Returns the first element in the list
	 * @return returns the first element in the list instance, null if empty
	 */
	
	@Override
	public E first() {
		if (size == 0)
			return null;
		
		return header.getNext().getElement();
	}

	
	/**
	 * Returns the last element in the list
	 * @return returns the last element in the list instance, null if empty
	 */
	
	@Override
	public E last() {
		if(size == 0)
			return null;
		
		return header.getPrev().getElement();
	}

	
	/**
	 * Returns the element at position index
	 * @param The desired index fo find in the list
	 * @return returns the element at index or an IndexOutOfBoundsException if invalid
	 * @throws IndexOutOfBoundsException
	 */
	
	@Override
	public E get(int index) throws IndexOutOfBoundsException{
		if (index < 0 || index > size -1)
			throw new IndexOutOfBoundsException("The specified index is not within the list's bounds");
		
		int i = 0;
		Node<E> temp = header.getNext();
		
		while (i != index) {
			temp = temp.getNext();
			i++;
		}
		
		return temp.getElement();
	}
	
	
	/**
	 * Removes all elements in the list
	 */
	
	@Override
	public void clear() {
		while(this.size != 0) {
			this.remove(0);
		}
	}

	
	/**
	 *Returns true if the list contains the specified element 
	 *@param The element to find if contained in the list
	 *@return true if the element is in the list, false otherwise
	 */
	
	@Override
	public boolean contains(E e) {
		Node<E> temp = header.getNext();
		
		for (int i = 0; i < size; i++) {
			if (temp.getElement().equals(e)) 
				return true;
			
			temp = temp.getNext();
		}
		
		return false;
	}

	
	/** 
	 * Indicates if the list's instance is empty.
	 * @return true if the instance's size is zero, false otherwise. 
	 */
	
	@Override
	public boolean isEmpty() {
		return size == 0;
	}


	/**
	 * Returns the index of the first occurrence of e in the list
	 * @param The element to find the first index of
	 * @return the index of e's position in the list, -1 if not in the list
	 */
	
	@Override
	public int firstIndex(E e) {
		Node<E> temp = header.getNext();
		
		for (int i = 0; i < size; i++) {
			if (temp.getElement().equals(e)) 
				return i;
			
			temp = temp.getNext();	
		}
		
		return -1;
	}

	
	/**
	 * Returns the index of the last occurrence of e in the list
	 * @param The element to find the last index of
	 * @return the index of e's position in the list, -1 if not in the list
	 */
	
	@Override
	public int lastIndex(E e) {
		Node<E> temp = header.getPrev();
		
		for (int i = size-1; i >= 0; i--) {
			if(temp.getElement().equals(e))
				return i;
		
			temp = temp.getPrev();
		}
		
		return -1;
	}
	

	//The following methods all deal with different iterators for the list, note that such 
	//Methods will throw an IndexOutOfboundsExp. when called upon an empty list. 
	
	/** 
	 * Returns a new SortedListReverseIterator object on the list starting from the last element on the list.
	 * @return a new ReverseIterator on the list Instance
	 */
	
	@Override
	public ReverseIterator<E> reverseIterator() {
		return new BackwardListIterator(size-1);
	}

	
	/** 
	 * Returns a new SortedListReverseIterator object on the list starting (inclusive) from position index and going backwards
	 * @param Index to start from, inclusive 
	 * @return a new ReverseIterator on the list Instance
	 * @throws IndexOutOfBoundsException if position index is invalid
	 */
	
	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		return new BackwardListIterator(index);
	}
	
	
	/** 
	 * Returns a new SortedListIterator object on the list
	 * @return a new ListIterator Instance
	 */
	
	@Override
	public Iterator<E> iterator() {
		return new ForwardListIterator(0);
	}
	
	
	/** 
	 * Returns a new SortedListIterator object on the list starting (inclusive) from position index 
	 * @param Index to start from, inclusive
	 * @return a new ListIterator Instance
	 * @throws IndexOutOfBoundsException if position index is invalid
	 */
	
	@Override
	public Iterator<E> iterator(int index) {
		return new ForwardListIterator(index);
	}
	
	
	
	//Auxiliary method to remove nodes
	
	
	/**
	 * Given a node ntr removes it from the current list instance
	 * @param ntr - the node to be removed
	 */
	
	private void removeNode(Node<E> ntr) {
	 
			if (this.size == 0) 
				return;
			
			ntr.getPrev().setNext(ntr.getNext());
			ntr.getNext().setPrev(ntr.getPrev());
			ntr.clean();
			size--;
	}

}
